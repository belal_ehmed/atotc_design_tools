﻿using UnityEngine;
using UnityEditor;

public class AToTCEditor : EditorWindow
{
    #region FIELDS
    //public
    public static string Tag = "AToTC Design Tools/";
    public string scaleLabel = "Scale Elements";
    public string cscaleLength;
    public float icscaleLength = 0;
    public float icscaleHeight;
    public float icscaleWidth;

    bool editGroupEnabled;
    float myFloat = 1.23f;

    //private
    Vector2 scrollPos;
    private System.Collections.Generic.List<GameObject> activePrefabs;
    private bool isHidden = false;
    private GameObject levelRoot;
    #endregion

    #region INIT
    void Awake()
    {
        InitLevelRoot();
        Debug.Log("AToTC Launched");
        activePrefabs = new System.Collections.Generic.List<GameObject>();
        OnActive_ResetCameraTransform();
    }

    void InitLevelRoot() 
    {
        if (GameObject.Find("levelRoot") == null)
            levelRoot = new GameObject("levelRoot");
        else
            levelRoot = GameObject.Find("levelRoot");
    }
    #endregion

    #region MENU BAR ITEMS
    //attribs
    [MenuItem("AToTC Design Tools/Level Tools")]
    public static void ShowWindow()
    {
        EditorWindow.GetWindow<AToTCEditor>("AToTC Design Editor");
    }

    [MenuItem("AToTC Design Tools/Docs")]//add docs item to the drop down menu
    public static void Docs()
    {
        Debug.Log("Init Complete");
        //Application.OpenURL("https://docs.google.com/document/d/1GzeUSDibyiuGsFGHiaIFDlUR6S0CLQbxjkaOuy7nltQ/edit#");
        //Application.OpenURL("https://sites.google.com/a/optera.digital/atotoc/");
        Application.OpenURL("https://docs.google.com/document/d/1KjSzhL0Kn3IccYgdSrfFwK9Cb0UH7_9WxN7GrSUidwI/edit?ts=5f05912a");
    }
    #endregion

    [ExecuteInEditMode]
    void DeleteObject() 
    {
        if (Event.current.commandName == "Delete") Debug.Log("Del: " + Selection.activeObject.name);
    }

    #region GUI
    //Editor window GUI functionality(buttons, labels etc.)

    private void OnGUI()
    {
        //set a vertical layout for GUI elements
        GUILayout.BeginVertical();
        scrollPos = EditorGUILayout.BeginScrollView(scrollPos);//create a scroll bar

        GUILayout.Label("Paper Level Design.\nProperty of Optera Digital");
        #region BASIC_SCALING
        GUILayout.Space(10.0f);
        //window stuff, GUILayout for labels buttons
        GUILayout.Label("Scale Elements", EditorStyles.boldLabel);
        if (GUILayout.Button("Default Scale Unit")) OnActive_ScaleElement("ScaleElement_BasicUnit");
        if (GUILayout.Button("Floor Tile Scale Unit")) OnActive_ScaleElement("ScaleElement_Floor");
        if (GUILayout.Button("Character Scale Unit")) OnActive_ScaleElement("ScaleElement_Character");

        if (GUILayout.Button("Platform Tile Scale Unit")) OnActive_ScaleElement("ScaleElement_Platform");
        //GUI.Label(new Rect(10, 40, 100, 40), GUI.tooltip);
        #endregion

        #region LEVEL_SCALING
        GUILayout.Space(10.0f);
        GUILayout.Label("Level Scale", EditorStyles.boldLabel);
        if (GUILayout.Button("Small Level")) OnActive_ScaleElement("Level_Scale1_1");
        if (GUILayout.Button("Medium Level")) OnActive_ScaleElement("Level_Scale2_1");
        if (GUILayout.Button("Large Level")) OnActive_ScaleElement("Level_Scale3_1");
        #endregion

        #region CUSTOM_SCALING
        GUILayout.Space(10.0f);
        GUILayout.Label("Custom Scales", EditorStyles.boldLabel);
        //EditorGUILayout for input fields and properties
        icscaleLength = EditorGUILayout.Slider("Length", icscaleLength, 1, 100);
        //cscaleLength = EditorGUILayout.TextField("Length",cscaleLength);
        icscaleWidth = EditorGUILayout.Slider("Width", icscaleWidth, 1, 100);
        icscaleHeight = EditorGUILayout.Slider("Height", icscaleHeight, 1, 100);
        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Reset Scales"))
        {
            icscaleLength = 1;
            icscaleWidth = 1;
            icscaleHeight = 1;
        }

        GUILayout.Space(10.0f);
        if (GUILayout.Button("Generate Custom Tile")) OnActive_GenerateCustomTile(icscaleLength, icscaleWidth, icscaleHeight);
        GUILayout.EndHorizontal();
        #endregion

        #region LEVEL
        GUILayout.Space(10.0f);
        GUILayout.Label("Level Design Elements", EditorStyles.boldLabel);
        if (GUILayout.Button("Floor Tile")) OnActive_LevelElement("Tile_Floor");
        if (GUILayout.Button("Corridor Tile Length 1")) OnActive_LevelElement("CorridorTile_length1");
        if (GUILayout.Button("Corridor Tile Length 6")) OnActive_LevelElement("CorridorTile_length6");
        if (GUILayout.Button("Corridor Tile Turn")) OnActive_LevelElement("CorridorTurn_length1");
        if (GUILayout.Button("Corridor Tile T-Section")) OnActive_LevelElement("CorridorTSection_length6");
        if (GUILayout.Button("Platform Static")) OnActive_LevelElement("Platform_Static");
        if (GUILayout.Button("Platform Horizontal Movement")) OnActive_LevelElement("PlatformDynamic_Horizontal1");
        if (GUILayout.Button("Platform Vertical Movement")) OnActive_LevelElement("PlatformDynamic_Vertical1");
        if (GUILayout.Button("Platform Autojump 1 way")) OnActive_LevelElement("Platform_AutoJump");
        if (GUILayout.Button("Platform Autojump 2 way")) OnActive_LevelElement("Platform_AutoJump2");
        if (GUILayout.Button("Platform Autojump 3 way")) OnActive_LevelElement("Platform_AutoJump3");
        if (GUILayout.Button("Platform Autojump 4 way")) OnActive_LevelElement("Platform_AutoJump4");
        if (GUILayout.Button("Ramp Low")) OnActive_LevelElement("Ramp_1");
        if (GUILayout.Button("Ramp High")) OnActive_LevelElement("Ramp_2");
        #endregion

        #region EDIT_TOOLS
        GUILayout.Space(10.0f);
        GUILayout.Label("Editing Tools", EditorStyles.boldLabel);

        GUILayout.BeginHorizontal();
        if (GUILayout.Button("Delete Selection(s)")) OnActive_DeletionDialog();
        //if (GUILayout.Button("Undo")) OnActive_DeleteSelection();
        GUILayout.EndHorizontal();

        if (GUILayout.Button("Hide/Unhide")) OnActive_Hide();
        GUILayout.Button("Save Paper Level(work in progress)");
        #endregion

        #region LEVEL_TESTING_TOOLS
        GUILayout.Space(10.0f);
        GUILayout.Label("Level Testing Tools", EditorStyles.boldLabel);
        if (GUILayout.Button("Test Player")) OnActive_LevelElement("Test_Player");
        if (GUILayout.Button("Reset Camera")) OnActive_ResetCameraTransform();
        #endregion

        EditorGUILayout.EndScrollView();
        GUILayout.EndVertical();
    }
    #endregion

    #region PRIVATES, HELPERS
    //GUI callbacks
    private void OnActive_ScaleElement(string assetName)
    {
        Debug.Log("REC_INDX: " + name);
        activePrefabs.Add(Instantiate(Resources.Load<GameObject>("Prefabs/ScalingElements/" + assetName)));
        activePrefabs[activePrefabs.Count - 1].gameObject.name = assetName;
        SetObjectParent(activePrefabs[activePrefabs.Count - 1].gameObject);
        Debug.Log("ACT" + activePrefabs.Count);
    }

    private void OnActive_GenerateCustomTile(float length, float width, float height)
    {
        Debug.Log("Custom width height: " + length + width + height);
        OnActive_ScaleElement("ScaleElement_Platform");
        activePrefabs[activePrefabs.Count - 1].gameObject.name = "CustomScaleTile_l" + length + "_w" + width + "_h" + height;
        activePrefabs[activePrefabs.Count - 1].transform.localScale = new Vector3(length, height, width);
        activePrefabs[activePrefabs.Count - 1].transform.localPosition = new Vector3(0f, activePrefabs[activePrefabs.Count - 1].transform.localScale.y * 0.5f, 0f);
        SetObjectParent(activePrefabs[activePrefabs.Count - 1].gameObject);
        Debug.Log("ACT" + activePrefabs.Count);
    }

    private void OnActive_LevelElement(string assetName)
    {
        Debug.Log("REC_INDX: " + name);
        activePrefabs.Add(Instantiate(Resources.Load<GameObject>("Prefabs/LevelElements/" + assetName)));
        activePrefabs[activePrefabs.Count - 1].gameObject.name = assetName;
        SetObjectParent(activePrefabs[activePrefabs.Count - 1].gameObject);
        Debug.Log("ACT" + activePrefabs.Count);
    }

    void OnActive_DeletionDialog()
    {
        if (isHidden)
            isHidden = false;
        if (Selection.objects.Length == 0)
            return;

        int option = EditorUtility.DisplayDialogComplex("Delete selected item(s)?",
            "Do you want to delete the selected item(s)? \nWARNING: You cannot undo this action.",
            "Yes",
            "No",
            "Cancel");

        switch (option)
        {
            // Yes.
            case 0:
                Debug.Log("Delete");
                OnActive_DeleteSelection();
                break;

            // No.
            case 1:
                Debug.Log("No");
                break;

            // Cancel.
            case 2:
                Debug.Log("Cancel");
                break;

            default:
                Debug.LogError("Unrecognized option.");
                break;
        }
    }

    private void OnActive_DeleteSelection()
    {

        Debug.Log("Selected: " + Selection.activeObject.name + " " + Selection.objects.Length);
        foreach (GameObject obj in Selection.objects)
        {
            GameObject.DestroyImmediate(obj);
            activePrefabs.Remove(obj);
        }
        Debug.Log("DELT: " + activePrefabs.Count);
    }

    private void OnActive_Hide()
    {
        /*
        Debug.Log("Active: "+activePrefabs.Count+" HID: "+isHidden);
        if (activePrefabs.Count == 0)
            return;
        for (int i = 0; i < activePrefabs.Count; i++)
            activePrefabs[i].SetActive(isHidden);
        Debug.Log("HIDE:" + activePrefabs.Count);
        */
        levelRoot.gameObject.SetActive(isHidden);
        isHidden = !isHidden;
    }

    //reset camera orientation for player
    private void OnActive_ResetCameraTransform()
    {
        Vector3 pos = new Vector3(-12, 12, -12);
        Vector3 rotation = new Vector3(30, 45, 0);
        Camera.main.transform.localPosition = pos;
        Camera.main.transform.localRotation = Quaternion.Euler(rotation);
        Debug.Log("CULL: "+Camera.main.cullingMask.ToString());
    }

    //sets levelRoot object as provided gameobject's parent
    private void SetObjectParent(GameObject obj) 
    {
        if (levelRoot == null)
            InitLevelRoot();

        obj.transform.SetParent(levelRoot.transform);
    }
    #endregion
}

