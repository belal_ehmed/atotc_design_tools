﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum CollisionType { Trigger=0, Collision }

public class Interactable : MonoBehaviour
{
    /*
     * Make it a generic entity
     * A rule for interaction
     * A message for reciever
     * VFX, SFX events
    */

    public CollisionType type;

    public virtual void Awake() 
    {
        if (type == CollisionType.Trigger)
        {
            gameObject.GetComponent<Collider>().isTrigger = true;
        }
    }

    public string triggerTag; 

    public virtual void OnCollisionEnter(Collision other){}

    public virtual void OnTriggerEnter(Collider other){}
}
