﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpTrigger : Interactable
{

    public override void OnTriggerEnter(Collider other)
    {
        base.OnTriggerEnter(other);
        Debug.Log("TRIGGERED");
        if(other.tag == triggerTag) 
        {
            other.GetComponent<ParabolaController>().FollowParabola();
        }
            
    }
}
