﻿using UnityEngine;

public class Platform : Interactable
{

    public override void Awake()
    {
        base.Awake();
        EnableJumpTriggers(false);   
    }

    public override void OnCollisionEnter(Collision other)
    {
        
        base.OnCollisionEnter(other);
        if(other.gameObject.tag == triggerTag) 
        {
            Debug.Log("COLL");
            EnableJumpTriggers(true);
        }
    }

    public void OnCollisionExit(Collision collision)
    {
        if (collision.gameObject.tag == triggerTag)
            EnableJumpTriggers(false);
    }

    private void EnableJumpTriggers(bool isEnabled) 
    {
        Debug.Log("ENABLED!");
        foreach (Transform child in transform)
        {
            Debug.Log("ENABLED");
            if (child.gameObject.GetComponent<Collider>()!=null)
                child.gameObject.GetComponent<Collider>().enabled = isEnabled;

        }

        
    }
}
