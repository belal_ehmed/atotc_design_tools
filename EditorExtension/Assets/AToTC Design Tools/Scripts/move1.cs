﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class move1 : MonoBehaviour
{


    public bool OnGround;
    public bool Onslope;
    public float slopeForce;
    public float slopeForceRayLength;
    public float velocity = 1;
    float turnspeed = 3;

    Quaternion targetRotation;
    Vector2 input;
    public float angle;
    Transform cam;

    //slope code
    public float height = 0.5f;
    public float heightpadding = 0.05f;
    public LayerMask ground;
    public float maxgroundangle = 120;
    public bool debug;
    public float jump_factor = 3;


    float groundAngle;

    Vector3 forward;
    RaycastHit hitinfo;
    bool grounded;


    bool isright;
    bool isleft;
    bool isup;
    bool isdown;
    float Rspeed = 90;

    bool is_destructable;
    int players = 1;
    public GameObject switch_script;
   public GameObject destroyobject;
    GameObject get_item;
    public Light topak_light;
    public Light topak_light2;
    //public Light topak_light3;
    //public Light topak_light4;

    public  Animator anim;

    

    // Start is called before the first frame update
    void Start()
    {

      


       
        OnGround = true;

        anim = GetComponent<Animator>();
        cam = Camera.main.transform;
        isdown = false;
        isleft = false;
        isup = false;
        isright = false;

        
    }

    // Update is called once per frame
    void Update()
    {
        Debug.Log("ONSLOPE: "+OnSlope());

        if ( !Input.anyKey )
        {          
            anim.Play("idle");            
        }
        
        getinput();
        jump();
        cal_direction();
        calculateForward();
        cal_groundAngle();
        check_ground();
      
       
        if (players == 1)
        {

            //if (Input.GetButtonDown("Fire1"))
            if (Input.GetKeyUp(KeyCode.Alpha0))
            {
                
                check_for_items();
                anim.Play("attack");
            }
            //if(Input.GetButtonUp("Fire1"))
            //{
            //    anim.Play("idle");
            //}
           

        }
        if (players == 2)
        {

            

        }

        if (Mathf.Abs(input.x) < 1 && Mathf.Abs(input.y) < 1)
            return;

        rotate();
        move();
        //FindObjectOfType<audiomanager>().stop("player_walk");



    }

    void OnCollisionEnter(Collision other)
    {

        
        if (other.gameObject.tag == "Ground")
        {
            Debug.Log("on ground");
            OnGround = true;
        }

        if (other.gameObject.tag == "slope")
        {
            Debug.Log("slope detected");
            Onslope = true;
        }
       
        if(other.gameObject.tag=="autojump")
        {
            GetComponent<Rigidbody>().velocity = Vector2.up * 4;
            OnGround = false;
            
        }



    }

    void OnCollisionStay(Collision other)
    {
        if (other.gameObject.tag == "item1")
        {
            
            is_destructable = true;
            get_item = other.gameObject;
        }
    }


    void OnCollisionExit(Collision other)
    {
        //Debug.Log("no slope");
        Onslope = false;

        is_destructable = false;
    }

    void move()
    {

        anim.Play("walk");





        //move cube up down left right

        if (OnSlope()) 
        {
            MoveNeo(Vector3.down * slopeForce*Time.deltaTime);
        }

        MoveNeo(Vector3.forward * velocity * Time.deltaTime);

        /*
        if (groundAngle >= maxgroundangle) return;
            transform.Translate(Vector3.forward * velocity * Time.deltaTime);
            */
        //transform.position += forward * velocity * Time.deltaTime;
    }

    //MOVEMENT
    void MoveNeo(Vector3 direction) 
    {
        transform.Translate(direction);
    }
    //MOVEMENT


    void jump()
    {
        //jump cube
        //limit jump from base (no jump on jump)
        if (Input.GetButtonDown("Jump") && OnGround)
        {
            GetComponent<Rigidbody>().velocity = Vector2.up * jump_factor;
            OnGround = false;
        }
    }

    void rotate()
    {
      

        //make rotation stop after one rotation click

        GameObject obj = GameObject.Find("character");


        if (Input.GetKey("up") && isup==false)
        {
            targetRotation = Quaternion.Euler(0, 90, 0);
            transform.localRotation = targetRotation;
            isdown = false;
            isleft = false;
            isup = true;
            isright = false;
           
           
        }
        if (Input.GetKey("down") && isdown == false)
        {
            targetRotation = Quaternion.Euler(0, -90, 0);
            transform.localRotation = targetRotation;
            isdown = true;
            isleft = false;
            isup = false;
            isright = false;
            
        }
        if (Input.GetKey("left") && isleft == false)
        {
            targetRotation = Quaternion.Euler(0, 0, 0);
            transform.localRotation = targetRotation;
            isdown = false;
            isleft = true;
            isup = false;
            isright = false;

            
        }
        if (Input.GetKey("right") && isright == false)
        {
            targetRotation = Quaternion.Euler(0, -180, 0);
            transform.localRotation = targetRotation;
            isdown = false;
            isleft = false;
            isup = false;
            isright = true;
           
        }

    }

    void getinput()
    {
        input.x = Input.GetAxisRaw("Horizontal");
        input.y = Input.GetAxisRaw("Vertical");
    }

    void cal_direction()
    {
        angle = Mathf.Atan2(input.x, input.y);
        angle = Mathf.Rad2Deg * angle;
        angle += cam.eulerAngles.y;
    }

    void calculateForward()
    {
        if (!grounded)
        {
            forward = transform.forward;
            return;
        }
        forward = Vector3.Cross(hitinfo.normal, transform.position);
    }

    void cal_groundAngle()
    {
        if (!grounded)
        {
            groundAngle = 90;
            return;
        }
        groundAngle = Vector3.Angle(hitinfo.normal, transform.forward);
    }

    void check_ground()
    {
        if (Physics.Raycast(transform.position, -Vector3.up, out hitinfo, height, ground))
        {
            if (Vector3.Distance(transform.position, hitinfo.point) < height)
            {
                transform.position = Vector3.Lerp(transform.position, transform.position + Vector3.up * height, 0 * Time.deltaTime);
            }
            grounded = true;
        }
        grounded = false;
    }

   
    void check_for_items()
    {

        if (is_destructable)
        {

            Debug.Log("oreo is near a destructable item ");
            //reference the item
            //destroy the item 

            
            if(get_item!=null)
            {
                //3d model method
                //particle system method

                //3d
                GameObject clone=Instantiate(destroyobject, get_item.transform.position, get_item.transform.rotation);
                //prt
                get_item.GetComponentInChildren<ParticleSystem>().Play();
                get_item.GetComponent<MeshRenderer>().enabled = false;

                //both
                Destroy(get_item.gameObject,3);


                 //3d 
                Destroy(clone, 3);

            }
            else
            {
                return;
            }
        }

    }

    private bool OnSlope() 
    {
        if (!OnGround) 
        {
            return false;
        }

        RaycastHit rayHit;
        Debug.DrawRay(transform.position, Vector3.down*slopeForceRayLength, Color.red);
        if(Physics.Raycast(transform.position, Vector3.down, out rayHit, slopeForceRayLength)) 
        {
            if(rayHit.normal != Vector3.up) 
            {
                return true;
            }
        }

        return false;
    }
}
