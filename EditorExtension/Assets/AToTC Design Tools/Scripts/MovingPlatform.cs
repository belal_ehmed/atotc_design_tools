﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovingPlatform : Interactable
{
    public bool repeatable = true;
    public float speed = 2.5f;
    public float duration = 3.0f;
    public GameObject looping_script;
    bool play = false;

    float startTime, totalDistance;
    float rate = 0;

    public Vector3 moveAxis;
    public Transform[] extents;

    public IEnumerator Start()
    {
        startTime = Time.time;
        totalDistance = Vector3.Distance(extents[0].position, extents[1].position);
        while (repeatable)
        {
            yield return RepeatLerp(extents[0].position, extents[1].position, duration);
            yield return RepeatLerp(extents[1].position, extents[0].position, duration);
        }
    }

    public override void OnCollisionEnter(Collision other)
    {
        base.OnCollisionEnter(other);
        if(other.gameObject.tag == triggerTag) 
        {
            other.gameObject.transform.parent = transform;
            //repeatable = true;

        }
    }

    public void OnCollisionExit(Collision other)
    {
        base.OnCollisionEnter(other);
        if (other.gameObject.tag == triggerTag)
        {
            other.gameObject.transform.parent = null;
        }
    }

    public IEnumerator RepeatLerp(Vector3 a, Vector3 b, float time)
    {
        float i = 0.0f;
        float rate = (1.0f / time) * speed;
        while (i < 1.0f)
        {
            i += Time.deltaTime * rate;
            this.transform.position = Vector3.Lerp(a, b, i);
            yield return null;
        }
    }
}
