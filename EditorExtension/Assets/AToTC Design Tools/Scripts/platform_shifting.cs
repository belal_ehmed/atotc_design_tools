﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class platform_shifting : MonoBehaviour
{

    public GameObject barrier1;
    public GameObject barrier3;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        //this.gameObject.SetActive(true);
    }
    void OnCollisionEnter(Collision other)
    {


        if (other.gameObject.tag == "hit")
        {
            Debug.Log("platform is up");
            barrier1.SetActive(false);
            barrier3.SetActive(false);
           
        }
        if(other.gameObject.tag=="onground")
        {
            //platform hits onground and opens barrier 3
            barrier1.SetActive(true);
            barrier3.SetActive(false);
        }
       

    }
    void OnCollisionStay(Collision other)
    {


        if (other.gameObject.tag == "hit")
        {
           
            barrier1.SetActive(false);

        }

        if (other.gameObject.tag == "onground")
        {

            barrier3.SetActive(false);
            barrier1.SetActive(true);

        }


    }


    void OnCollisionExit(Collision other)
    {
        Debug.Log("platform is down");
        barrier1.SetActive(true);
        barrier3.SetActive(true);

       //set both platform and top barrier to true

    }
}
